<?php

/**
 * @file
 * Enables classes to be used for additional template variable preprocessing on a bodypart type.
 */

define('BODYPART_OOP_DEFAULT_CLASS_PREFIX', 'Drupal_BodypartTypes_');

/**
 * Implementation of hook_form_alter
 */
function bodypart_oop_form_alter(&$aForm, $aFormState, $sFormId) {
  if ($sFormId == 'oop_settings') {
    $aForm['oop']['bodypart_oop'] = array(
      '#type' => 'fieldset',
      '#title' => t('Bodyparts'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $aForm['oop']['bodypart_oop']['bodypart_oop_class_prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Prefix for classes that extend bodypart types'),
      '#default_value' => variable_get('bodypart_oop_class_prefix', BODYPART_OOP_DEFAULT_CLASS_PREFIX),
    );
  }
}

/**
 * Return an object instance of a bodypart type
 */
function bodypart_oop_get_object($sBodypartType) {
  $sClassName = bodypart_oop_get_class_name($sBodypartType);
  return oop_get_object($sClassName);
}

/**
 * Get the classname for a bodypart type
 */
function bodypart_oop_get_class_name($sBodypartType) {
  $sClassPrefix = variable_get('bodypart_oop_class_prefix', BODYPART_OOP_DEFAULT_CLASS_PREFIX);
  $sClassName = $sClassPrefix . ucfirst(oop_delimited_to_camel($sBodypartType));
  return $sClassName;
}

/**
 * Implementation of hook_preprocess_bodypart_content
 */
function bodypart_oop_preprocess_bodypart_content(&$aVars) {
  $aItem =& $aVars['element']['#flexifield_item'];
  if (!$aItem || !$aItem['type']) {return;}
  $o = bodypart_oop_get_object($aItem['type']);
  if ($o && is_callable(array($o, 'preprocessContent'))) {
    $o->preprocessContent($aItem, $aVars);
  }
}

/**
 * Base class that bodypart classes can inherit from. No implementation yet.
 */
class Drupal_Bodypart {
}